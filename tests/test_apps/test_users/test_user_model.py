import pytest

TEST_EMAIL = 'lennon@thebeatles.com'
TEST_PASSWORD = 'johnpassword'


@pytest.mark.django_db()
def test_create_user(django_user_model):
    """Test user object can be created through manager."""
    user = django_user_model.objects.create_user(
        email=TEST_EMAIL,
        password=TEST_PASSWORD,
    )
    assert django_user_model.objects.count() == 1
    assert user.email == TEST_EMAIL
    assert user.is_active
    assert not user.is_superuser
    assert not user.is_staff

    with pytest.raises(ValueError, match='The Email must be set'):
        django_user_model.objects.create_user(
            email=None,
            password=TEST_PASSWORD,
        )


@pytest.mark.django_db()
def test_create_superuser(django_user_model):
    """Test user object can be created through manager."""
    admin = django_user_model.objects.create_superuser(
        email=TEST_EMAIL,
        password=TEST_PASSWORD,
    )
    assert django_user_model.objects.count() == 1
    assert admin.is_staff
    assert admin.is_superuser

    with pytest.raises(ValueError, match='Superuser must have is_staff=True.'):
        django_user_model.objects.create_superuser(
            email=TEST_EMAIL,
            password=TEST_PASSWORD,
            is_staff=False,
        )

    with pytest.raises(
        ValueError,
        match='Superuser must have is_superuser=True.',
    ):
        django_user_model.objects.create_superuser(
            email=TEST_EMAIL,
            password=TEST_PASSWORD,
            is_superuser=False,
        )
