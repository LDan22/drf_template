import json

import pytest
from rest_framework.reverse import reverse_lazy

users_list_url = reverse_lazy('users:list')


@pytest.mark.django_db()
def test_users_list_api(api_client, users):
    """Test users are listed correctly."""
    response = api_client().get(users_list_url)

    assert response.status_code == 200
    response_data = json.loads(response.content)
    assert len(response_data['results']) == len(users)
