import pytest

from server.apps.users.logic.selectors import UserSelector
from server.apps.users.logic.services import UserService


@pytest.mark.django_db()
def test_user_selector_get_by_email_success():
    """Test can select user by email."""
    user_service = UserService()
    user_service.create(
        email='test@email.com',
        password='test_pass123',
    )
    user_selector = UserSelector()
    user = user_selector.get_by_email('test@email.com')
    assert user
