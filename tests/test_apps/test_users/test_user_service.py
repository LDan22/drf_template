import pytest

from server.apps.users.logic.services import UserService


@pytest.mark.django_db()
def test_user_service_create_user_success():
    """Test user is created successfully."""
    user_service = UserService()
    user_service.create(
        email='test@email.com',
        password='test_pass123',
    )
