import pytest
from mixer.backend.django import mixer


@pytest.fixture()
def default_user(django_user_model):
    return django_user_model.objects.create_user(
        email='test@email.com',
        password='test_pass123',
    )


@pytest.fixture()
def users(django_user_model):
    """Mock list of users."""
    user_ids = (pk + 1 for pk in range(5))
    return mixer.cycle(5).blend(django_user_model, id=user_ids)
