import pytest
from django.core.exceptions import (
    ImproperlyConfigured as DjangoImproperlyConfigured,
)
from django.core.exceptions import ValidationError as DjangoValidationError
from django.test import RequestFactory
from rest_framework import status
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

from server.apps.core.mixins import ApiErrorsMixin


def service_that_raises_django_validation_error():
    """Raise known by mixin error."""
    raise DjangoValidationError(message='error')


def service_that_raises_unknown_error():
    """Raise unknown by mixin error."""
    raise DjangoImproperlyConfigured()


class ApiUnderTestKnownError(ApiErrorsMixin, GenericAPIView):
    """Mock view for raising known error."""

    def post(self, request):
        """Post request for simulating action."""
        service_that_raises_django_validation_error()
        return Response(status=status.HTTP_202_ACCEPTED)


class ApiUnderTestUnknownError(ApiErrorsMixin, GenericAPIView):
    """Mock view for raising unknown error."""

    def post(self, request):
        """Post request for simulating action."""
        service_that_raises_unknown_error()
        return Response(status=status.HTTP_202_ACCEPTED)


def test_api_errors_mixin(api_client):
    """Test exception is handled by mixin."""
    factory = RequestFactory()
    request_factory = factory.post('/')
    response = ApiUnderTestKnownError.as_view()(request_factory)

    assert response.status_code == 400


def test_api_errors_mixin_unknown_error(api_client):
    """Test unknown error raises."""
    factory = RequestFactory()
    request_factory = factory.post('/')

    with pytest.raises(DjangoImproperlyConfigured):
        ApiUnderTestUnknownError.as_view()(request_factory)
