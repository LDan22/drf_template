import pytest
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Permission

from server.apps.core.selectors import ModelSelector
from server.apps.core.services import ModelService


class UserSelector(ModelSelector):
    """
    User model selector for tests.

    Used for testing simple selection methods and prefetch related.
    """

    model_class = get_user_model()


class PermissionSelector(ModelSelector):
    """
    Permission model selector for tests.

    Used for testing select related.
    """

    model_class = Permission


class UserService(ModelService):
    """User model service for testing base service methods."""

    model_class = get_user_model()


@pytest.fixture()
def user_selector():
    return UserSelector()


@pytest.fixture()
def permission_selector():
    return PermissionSelector()


@pytest.fixture()
def user_service():
    return UserService()
