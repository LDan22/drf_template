from collections import OrderedDict

import pytest
from rest_framework.request import Request
from rest_framework.test import APIRequestFactory

from server.apps.core.pagination import ApiLimitOffsetPagination

factory = APIRequestFactory()
request = Request(factory.get('/', {'limit': 5}))
pagination = ApiLimitOffsetPagination()


@pytest.mark.django_db()
def test_paginator_builds_correct_response(users):
    queryset = pagination.paginate_queryset(users, request)
    response = pagination.get_paginated_response(queryset).data
    assert response == {
        'results': users,
        'links': OrderedDict(
            [
                ('first', 'http://testserver/?limit=5'),
                ('last', 'http://testserver/?limit=5&offset=5'),
                ('next', None),
                ('prev', None),
            ],
        ),
        'meta': {
            'pagination': {
                'count': 5,
                'limit': 5,
                'offset': 0,
            },
        },
    }


@pytest.mark.django_db()
def test_paginator_no_data():
    queryset = pagination.paginate_queryset([], request)
    response = pagination.get_paginated_response(queryset).data
    assert response == {
        'results': [],
        'links': OrderedDict(
            [
                ('first', None),
                ('last', None),
                ('next', None),
                ('prev', None),
            ],
        ),
        'meta': {
            'pagination': OrderedDict(
                [('count', 0), ('limit', 5), ('offset', 0)],
            ),
        },
    }
