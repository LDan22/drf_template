import pytest
from django.contrib.auth.models import Group
from mixer.backend.django import mixer


@pytest.mark.django_db()
def test_model_service_create_success(django_user_model, user_service):
    """Test can create model instance using service."""
    user_service.create(email='test_user@email.com', password='test_password')
    assert django_user_model.objects.count() == 1


@pytest.mark.django_db()
def test_model_service_create_with_m2m(django_user_model, user_service):
    """Test can create model instance with m2m fields provided."""
    group = Group.objects.create(name='test')
    user_service.create(
        email='test_user@email.com',
        password='test_password',
        groups=[group],
    )
    assert django_user_model.objects.count() == 1


@pytest.mark.django_db()
def test_model_service_update(default_user, user_service):
    """Test updating entity is performed correct."""
    group = Group.objects.create(name='test')
    updated_user = user_service.update(
        default_user,
        email='new@email.com',
        groups=[group],
    )
    assert updated_user.email == 'new@email.com'
    assert updated_user.groups.count() == 1


@pytest.mark.django_db()
def test_update_only_m2m_fields(default_user, user_service):
    """Test can update only m2m fields."""
    group = Group.objects.create(name='test')
    updated_user = user_service.update(
        default_user,
        groups=[group],
    )
    assert updated_user.groups.count() == 1


@pytest.mark.django_db()
def test_model_service_delete_success(django_user_model, user_service):
    """Test can delete instance using model service."""
    user = mixer.blend(django_user_model)
    assert django_user_model.objects.count() == 1

    user_service.delete_by_id(user.id)
    assert django_user_model.objects.count() == 0


@pytest.mark.django_db()
def test_delete_entity(default_user, user_service):
    """Test entity is deleted."""
    deleted = user_service.delete(default_user)
    assert deleted[0] == 1
