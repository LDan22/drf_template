"""
Main URL mapping configuration file.

Include other URLConfs from external apps using method `include()`.

It is also a good practice to keep a single URL to the root index page.

This examples uses Django's default media
files serving technique in development.
"""

from django.conf import settings
from django.contrib import admin
from django.urls import include, path
from health_check import urls as health_urls

from server.apps.core.swagger.schema_view import APISchemaView
from server.apps.users import urls as users_urls

urlpatterns = [
    path('api/v1/', include([
        path('users/', include(users_urls, namespace='users')),

        # swagger
        path(
            'swagger/',
            APISchemaView.with_ui('swagger', cache_timeout=0),
            name='schema-swagger-ui',
        ),
    ])),

    # django-admin:
    path('admin/', admin.site.urls),

    # Health checks:
    path('health/', include(health_urls)),
]

if settings.DEBUG:  # pragma: no cover
    import debug_toolbar  # noqa: WPS433
    from django.conf.urls.static import static  # noqa: WPS433

    urlpatterns += [
        path('__debug__/', include(debug_toolbar.urls)),
    ]
    urlpatterns += static(
        settings.MEDIA_URL,
        document_root=settings.MEDIA_ROOT,
    )
