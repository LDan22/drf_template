from django.urls import path

from server.apps.users.views import UsersListAPIView

app_name = 'users'

urlpatterns = [
    path('', UsersListAPIView.as_view(), name='list'),
]
