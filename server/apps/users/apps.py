from django.apps import AppConfig


class UsersConfig(AppConfig):
    """Users app configuration class."""

    default_auto_field = 'django.db.models.BigAutoField'
    name = 'server.apps.users'
