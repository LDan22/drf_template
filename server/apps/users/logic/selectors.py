from django.contrib.auth import get_user_model

from server.apps.core.selectors import ModelSelector


class UserSelector(ModelSelector):
    """Selector for user model."""

    model_class = get_user_model()

    def get_by_email(self, email):
        return self.model_class.objects.get(email=email)

    def get_all_active(self):
        return self.filter_by(is_active=True)
