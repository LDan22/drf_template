from django.contrib.auth import get_user_model

from server.apps.core.services import ModelService


class UserService(ModelService):
    """Service for user model."""

    model_class = get_user_model()

    def create(self, email, password):
        """
        User model requires special logic for creating new user.

        Password should be stored hashed, therefore we use method from
        `UserManager` for creating new user.
        """
        return self.model_class.objects.create_user(
            email=email,
            password=password,
        )
