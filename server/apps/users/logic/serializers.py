from django.contrib.auth import get_user_model
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    """User output serializer."""

    class Meta(object):
        model = get_user_model()
        fields = ('id', 'email', 'is_active', 'is_staff')
