from django.contrib.auth.forms import UserChangeForm, UserCreationForm

from server.apps.users.models import CustomUser


class CustomUserCreationForm(UserCreationForm):
    """User create form for User model in admin panel."""

    class Meta(object):
        model = CustomUser
        fields = ('email',)


class CustomUserChangeForm(UserChangeForm):
    """User change form for User model in admin panel."""

    class Meta(object):
        model = CustomUser
        fields = ('email',)
