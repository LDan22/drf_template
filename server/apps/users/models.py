from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import gettext_lazy as _

from server.apps.users.logic.managers import CustomUserManager


class CustomUser(AbstractUser):
    """
    Project user model.

    Extends core AbstractUser. Need to extend it for future changes.
    https://docs.djangoproject.com/en/3.2/topics/auth/customizing/#using-a-custom-user-model-when-starting-a-project
    """

    username = None
    email = models.EmailField(_('email address'), unique=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = CustomUserManager()  # noqa: WPS110

    def __str__(self):
        return self.email
