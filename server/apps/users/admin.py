from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import gettext_lazy as _

from server.apps.users.logic.forms import (
    CustomUserChangeForm,
    CustomUserCreationForm,
)
from server.apps.users.models import CustomUser

EMAIL_FIELD = 'email'
PASSWORD_FIELD = 'password'  # noqa: S105
FIRST_NAME_FIELD = 'first_name'
LAST_NAME_FIELD = 'last_name'
IS_STAFF_FIELD = 'is_staff'
IS_ACTIVE_FIELD = 'is_active'
LAST_LOGIN = 'last_login'
DATE_JOINED = 'date_joined'
FIELDS = 'fields'


@admin.register(CustomUser)
class CustomUserAdmin(UserAdmin):
    """Admin panel for `CustomUser` model."""

    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = CustomUser
    list_display = (EMAIL_FIELD, IS_STAFF_FIELD, IS_ACTIVE_FIELD)
    list_filter = (EMAIL_FIELD, IS_STAFF_FIELD, IS_ACTIVE_FIELD)
    fieldsets = (
        (
            None,
            {FIELDS: (EMAIL_FIELD, PASSWORD_FIELD)},
        ),
        (
            _('Personal info'),
            {FIELDS: (FIRST_NAME_FIELD, LAST_NAME_FIELD)},
        ),
        (
            _('Permissions'),
            {FIELDS: (IS_STAFF_FIELD, IS_ACTIVE_FIELD)},
        ),
        (
            _('Important dates'),
            {FIELDS: (LAST_LOGIN, DATE_JOINED)},
        ),
    )

    add_fieldsets = (
        (
            None, {
                'classes': ('wide',),
                FIELDS: (
                    EMAIL_FIELD,
                    'password1',
                    'password2',
                    IS_STAFF_FIELD,
                    IS_ACTIVE_FIELD,
                ),
            },
        ),
    )
    search_fields = (EMAIL_FIELD,)
    ordering = (EMAIL_FIELD,)
