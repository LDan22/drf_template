from abc import ABC, abstractmethod

from server.apps.core.models_utils import (
    get_to_many_model_fields,
    set_fields_values,
    set_to_many_fields_values,
)


class Service(ABC):
    """
    Abstract service class.

    Defines basic creating, updating, deleting operations.
    """

    @abstractmethod
    def create(self, **kwargs):
        """Create new entity from provided data in kwargs."""

    @abstractmethod
    def update(self, entity, **kwargs):
        """Update entity with provided data in kwargs."""

    @abstractmethod
    def delete_by_id(self, entity_id):
        """Delete entity by its id."""

    @abstractmethod
    def delete(self, entity):
        """Delete a entity."""


class ModelService(Service):
    """
    Service class for a DjangoORM models.

    Here are defined methods for logic - from simple model creation to complex
    cross-cutting concerns, to calling external services & tasks.
    """

    model_class = None

    def __init__(self):
        self._queryset = self.model_class.objects

    def create(self, **validated_data):
        to_many_fields, validated_data = _extract_to_many_fields(
            self.model_class,
            **validated_data,
        )
        entity = self._queryset.create(**validated_data)
        set_to_many_fields_values(entity, to_many_fields)
        return entity

    def update(self, entity, **validated_data):
        to_many_fields, validated_data = _extract_to_many_fields(
            self.model_class,
            **validated_data,
        )
        set_fields_values(entity, validated_data)
        entity.save(update_fields=list(validated_data.keys()))
        set_to_many_fields_values(entity, to_many_fields)
        return entity

    def delete_by_id(self, entity_id):
        return self._queryset.get(id=entity_id).delete()

    def delete(self, entity):
        return entity.delete()


def _extract_to_many_fields(model_class, **validated_data):
    """
    Extract m2m key-value fields pairs from validated_data.

    Remove many-to-many relationships from data.
    They are not valid arguments to the default `.create()` method,
    as they require that the instance has already been saved.
    """
    to_many_fields = {}
    for field_name in get_to_many_model_fields(model_class):
        if field_name in validated_data:
            to_many_fields[field_name] = validated_data.pop(field_name)
    return to_many_fields, validated_data
