from contextlib import suppress
from itertools import repeat

from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.db import IntegrityError
from rest_framework import exceptions


def isinstance_with_return(exception, match_keys):
    """Check if object is instance from list and return matched type."""
    for matcher in match_keys:
        if isinstance(exception, matcher):
            return matcher
    return None


def unpack_single_key(key, key_val):
    """Unpack elements from tuple and return set of key: value pairs."""
    if isinstance(key, tuple):
        return zip(key, repeat(key_val))
    return zip((key,), repeat(key_val))


def unpack_dict_keys(dictionary: dict):
    """Unpacks tuple keys of a dict."""
    exception_tuples = []
    for key, key_val in dictionary.items():
        unpacked_pairs = unpack_single_key(key, key_val)
        exception_tuples.extend(unpacked_pairs)
    return dict(exception_tuples)


def get_first_matching_attr(dictionary, *attrs, default=None):
    """Returns first matching attribute from a dict."""
    for attr in attrs:
        with suppress(AttributeError):
            return getattr(dictionary, attr)

    return default


def get_error_message(exc):
    """Returns error message from exception."""
    try:
        return exc.message_dict
    except AttributeError:
        error_msg = get_first_matching_attr(exc, 'message', 'messages')

        if isinstance(error_msg, list):
            error_msg = ', '.join(error_msg)

        if error_msg is None:
            error_msg = str(exc)

        return error_msg


class ApiErrorsMixin(object):
    """
    Mixin that transforms Django and Python exceptions into rest_framework ones.

    Without the mixin, they return 500 status code which is not desired.
    Should be used with an drf view class.
    """

    expected_exceptions = {
        (
            ValueError,
            ValidationError,
            IntegrityError,
        ): exceptions.ValidationError,
        PermissionError: exceptions.PermissionDenied,
        ObjectDoesNotExist: exceptions.NotFound,
    }

    def handle_exception(self, exc):
        """Override handle exception method and format exceptions."""
        unpacked_expected_exceptions = unpack_dict_keys(
            self.expected_exceptions,
        )

        exception_type = isinstance_with_return(
            exc,
            list(unpacked_expected_exceptions.keys()),
        )

        if exception_type:
            drf_exception_class = unpacked_expected_exceptions[exception_type]
            drf_exception = drf_exception_class(get_error_message(exc))
            return super().handle_exception(drf_exception)

        return super().handle_exception(exc)
