from abc import ABC, abstractmethod


class Selector(ABC):
    """
    Abstract selector class.

    Defines basic selecting operations.
    """

    @abstractmethod
    def get_all(self):
        """Get all entities."""

    @abstractmethod
    def get_by_id(self, entity_id):
        """Get one entity by its id."""


class ModelSelector(Selector):
    """
    Selector class for a DjangoORM model.

    Here are defined methods for logic around fetching data from database.
    """

    model_class = None

    def __init__(self):
        self._queryset = self.model_class.objects

    def with_select_related(self, *fields):
        """
        Apply .select_related() to queryset.

        Parameters
        ----------
        fields : str
            Variable length field list. Should provide only 'to one' fields: FK,
            OneToOne.

        Returns
        -------
        `Selector` instance with .select_related() applied to queryset.
        """
        self._queryset = self._queryset.select_related(*fields)
        return self

    def with_prefetch_related(self, *fields):
        """
        Apply .prefetch_related() to queryset.

        Parameters
        ----------
        fields : str
            Variable length field list. Should provide only related fields.
            Is recommended to use fields of types: ManyToMany.

        Returns
        -------
        `Selector` instance with .prefetch_related() applied to queryset.
        """
        self._queryset = self._queryset.prefetch_related(*fields)
        return self

    def get_all(self):
        return self._queryset.all()

    def get_by_id(self, entity_id):
        return self._queryset.get(id=entity_id)

    def get_one_by(self, **fields):
        return self._queryset.get(**fields)

    def filter_by(self, **fields):
        return self._queryset.filter(**fields)
