from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions

API_DESCRIPTION = openapi.Info(
    title='Backend API',
    default_version='v1',
    description='APP APIs',
    contact=openapi.Contact(email='admin@email.com'),
    license=openapi.License(name='APP License'),
)

APISchemaView = get_schema_view(
    info=API_DESCRIPTION,
    public=True,
    permission_classes=(permissions.AllowAny,),
)
