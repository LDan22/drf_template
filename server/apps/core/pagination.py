from collections import OrderedDict

from django.conf import settings
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.utils.urls import remove_query_param, replace_query_param
from rest_framework.views import Response


class ApiLimitOffsetPagination(LimitOffsetPagination):
    """
    A limit/offset based style.

    For example:
        http://api.example.org/accounts/?limit=100
        http://api.example.org/accounts/?offset=400&limit=100
    """

    limit_query_param = 'limit'
    offset_query_param = 'offset'
    max_limit = settings.MAX_PAGE_SIZE

    def get_last_link(self):
        if self.count == 0:
            return None

        url = self.request.build_absolute_uri()
        url = replace_query_param(url, self.limit_query_param, self.limit)

        offset = self._calculate_offset_for_last_link()

        return replace_query_param(url, self.offset_query_param, offset)

    def get_first_link(self):
        if self.count == 0:
            return None

        url = self.request.build_absolute_uri()
        return remove_query_param(url, self.offset_query_param)

    def get_paginated_response(self, response_data):
        return Response(
            {
                'results': response_data,
                'meta': {
                    'pagination': OrderedDict(
                        [
                            ('count', self.count),
                            ('limit', self.limit),
                            ('offset', self.offset),
                        ],
                    ),
                },
                'links': OrderedDict(
                    [
                        ('first', self.get_first_link()),
                        ('last', self.get_last_link()),
                        ('next', self.get_next_link()),
                        ('prev', self.get_previous_link()),
                    ],
                ),
            },
        )

    def _calculate_offset_for_last_link(self):
        return (self.count // self.limit) * self.limit
