SWAGGER_SETTINGS = {
    'DEFAULT_PAGINATOR_INSPECTORS': [
        'server.apps.core.swagger.inspectors.ApiLimitOffsetPaginatorInspector',
    ],
}

REDOC_SETTINGS = {
    'LAZY_RENDERING': False,
}
