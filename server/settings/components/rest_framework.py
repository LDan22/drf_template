MAX_PAGE_SIZE = 100
PAGE_SIZE = 10

REST_FRAMEWORK = {
    'DEFAULT_PAGINATION_CLASS':
        'server.apps.core.pagination.ApiLimitOffsetPagination',
    'DEFAULT_FILTER_BACKENDS': (
        'django_filters.rest_framework.DjangoFilterBackend',
    ),
    'PAGE_SIZE': PAGE_SIZE,
}

CORS_ORIGIN_WHITELIST = (
    'http://localhost:3000',
)
